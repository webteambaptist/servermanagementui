﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServerManagementTool.Models
{
    public class SubmitResult
    {
        public string Status { get; set; }
    }
}
