﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServerManagementTool.Models
{
    public class ColorLegend
    {
        public string ColorCode { get; set; }
        public string ColorCodeMeaning { get; set; }
    }
}
