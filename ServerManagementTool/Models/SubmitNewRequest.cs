﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServerManagementTool.Models
{
    public class SubmitNewRequest
    {
        public string RequestKey { get; set; }
        public string RequestValue { get; set; }
    }

    public class UpdateHeaderDetails
    {
        public string HeaderName { get; set; }
        public string HeaderDisplayName { get; set; }
        public string Type { get; set; }
        public string NotesAboutField { get; set; }
        public List<HeaderDetailOptions> Options { get; set; }
        public string ExistingValue { get; set; }
    }
}
