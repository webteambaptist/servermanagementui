﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServerManagementTool.Models
{
    public class HeaderDetails
    {
        public string HeaderName { get; set; }
        public string HeaderDisplayName { get; set; }
        public string Type { get; set; }
        public string NotesAboutField { get; set; }
        public List<HeaderDetailOptions> Options { get; set; }
    }

    public partial class HeaderDetailOptions
    {
        public string DropDownOption { get; set; }
    }
}
