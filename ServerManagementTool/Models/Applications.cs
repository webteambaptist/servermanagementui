﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServerManagementTool.Models
{
    public class Applications
    {
        public string AppName { get; set; }
        public string TeamName { get; set; }
        public string ServerName { get; set; }
        public string AppOwner { get; set; }
        public string AppType { get; set; }
        public string AppEnvironment { get; set; }
        public string AppStatus { get; set; }
        public string AppUrl { get; set; }
        public string ApprovalStatus { get; set; }
    }

    //public partial class AdditionalAppDetails
    //{
    //    public string AppName { get; set; }
    //    public string TeamName { get; set; }
    //    public string ServerName { get; set; }
    //    public string AppOwner { get; set; }
    //    public string AppType { get; set; }
    //    public string AppEnvironment { get; set; }
    //    public string AppPathonServer { get; set; }
    //    public string AppUrl { get; set; }
    //    public bool ConnectedDB { get; set; }
    //    public string DBAlias { get; set; }
    //    public string DBName { get; set; }
    //    public string Notes { get; set; }
    //}
    public partial class FinalAdditionalAppDetails
    {
        public string DetailHeader { get; set; }
        public string DetailValue { get; set; }
    }
}
