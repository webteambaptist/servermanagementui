﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServerManagementTool.Models
{
    public class Servers
    {
        public string ServerName { get; set; }
        public string TeamName { get; set; }
        public string OperatingSystem { get; set; }
        public string ServerStatus { get; set; }
        public string AltirisPatchingGroup { get; set; }
        public string InAltirisForPatching { get; set; }
        public string ServerNotes { get; set; }
        public string Environment { get; set; }
        public string ApprovalStatus { get; set; }
    }

    public partial class ServerDetails
    {
        public string DetailHeader { get; set; }
        public string DetailValue { get; set; }
    }
}
