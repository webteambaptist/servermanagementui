﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServerManagementTool.Models
{
    public class Teams
    {
        public string TeamName { get; set; }
    }

    public class AdminUserForTeam
    {
        public string AdminUsers { get; set; }
    }

    //public class TeamDetails
    //{
    //    public string[] GroupHeaders { get; set; }
    //}
}
