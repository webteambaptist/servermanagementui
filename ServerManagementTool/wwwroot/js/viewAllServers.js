﻿var serversTable;

$(document).ready(function () {
    if (window.location.href.indexOf("serverManagementTool") > -1 || window.location.href.indexOf("ServerManagementTool") > -1) {
        mainUrl = '/ServerManagementTool/';
    } else {
        mainUrl = '../';
    }

    GenerateServersTable();
});

function GenerateServersTable() {
    serversTable = $("#serverTable").DataTable({
        "dom": '<"top"l>prt<"bottom"ip><"clear">',
        "destroy": true,
        "bAutoWidth": true,
        "ajax": {
            "url": mainUrl + "Home/GetAllServers",
            "error": function (errResp) {
                alert("There was an issue processing your request. Please try again.");
            },
            "dataSrc": function (json) {
                var data = JSON.parse(json.data);
                return data;
            },
            "type": "GET",
            "datatype": "json"
        },
        "language": {
            "emptyTable": "There are currently no Applications to display."
        },
        "columns": [
            {
                "data": "ServerName",
                sDefaultContent: "",
                "render": function (name, type, data, meta) {
                   
                    return "<h5><i><u>" + data.ServerName + "</u></i></h5><p><b>Operating System: </b><br>" + data.OperatingSystem + "</p><p><b>App Environment: </b><br>" + data.Environment + "</p>";
                }
            },
            { "data": "TeamName" },
            {
                "data": "ServerStatus",
                sDefaultContent: "",
                "render": function (name, type, data, meta) {
                    return "<p><b>Server Status: </b><br>" + data.ServerStatus + "</p>" + "<p><b>Approval Status: </b><br>" + data.ApprovalStatus + "</p>";
                }
            },
            {
                "data": "InAltirisForPatching",
                sDefaultContent: "",
                "render": function (name, type, data, meta) {
                    switch (data.InAltirisForPatching) {
                    case "True":
                        return '<i class="far fa-check-circle fa-2x" style="color: green !important;"></i>';
                    case "False":
                            return '<i class="far fa-times-circle fa-2x" style="color: red !important;"></i>';
                    }
                    return "<h5><i><u>" + data.ServerName + "</u></i></h5><p><b>App Environment: </b><br>" + data.Environment + "</p>";
                }
            },
            { "data": "AltirisPatchingGroup" },
            { "data": "ServerNotes" }
        ],
        "columnDefs": [
            {
                targets: 0,
                className: 'text-center',
                width: "18%"
            },
            {
                targets: 1,
                className: 'text-center'
            },
            {
                targets: 2,
                className: 'text-center'
            },
            {
                targets: 3,
                className: 'text-center'
            },
            {
                targets: 4,
                className: 'text-center'
            },
            {
                targets: 5,
                className: 'text-center'
            }
        ],
        "pageLength": 10,
        "pagingType": "full_numbers",
        order: [[0, 'asc']],
        rowCallback: function (row, data, index) {
            switch (data.Environment) {
            case "Dev":
                $(row).find('td:eq(0)').css('background-color', '#CCABDE');
                break;
            case "QA":
                $(row).find('td:eq(0)').css('background-color', '#B1C8F7');
                break;
            case "Production":
                $(row).find('td:eq(0)').css('background-color', '#66D589');
                break;
            }

            switch (data.ServerStatus) {
            case "Active":
                $(row).find('td:eq(2)').css('background-color', '#32c532');
                break;
            case "Not Active":
                $(row).find('td:eq(2)').css('background-color', '#ff0000');
                break;
            case "On Decommission List":
                $(row).find('td:eq(2)').css('background-color', '#FFBA69');
                break;
            case "N/A":
                $(row).find('td:eq(2)').css('background-color', '');
                break;
            }
        },
        responsive: true
    });
}

$("#searchBox").keyup(function () {
    serversTable.rows().search(this.value).draw();
});
