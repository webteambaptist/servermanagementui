﻿var applicationsTable;
var serversTable;
var isAdmin;
var selectedEnvironment;
var selectedEnvironmentServer;
var selectedAppType;
$(document).ready(function () {

    $('.modal').on("hidden.bs.modal", function (e) { //fire on closing modal box
        if ($('.modal:visible').length) { // check whether parent modal is opened after child modal close
            $('body').addClass('modal-open'); // if open mean length is 1 then add a bootstrap css class to body of the page
        }
    });

    if (window.location.href.indexOf("serverManagementTool") > -1 || window.location.href.indexOf("ServerManagementTool") > -1) {
        mainUrl = '/ServerManagementTool/';
    } else {
        mainUrl = '../';
    }
    $('#btnNewServerRequest').hide();
    $('#btnNewApplicationRequest').hide();
    $('#allEnvironmentsList').hide();
    $('#allAppTypeList').hide();
    $('#requesterInfo').hide();

    $('#serverDetailsSection').hide();

    var teamName = sessionStorage.getItem("TeamName");
    var appTypeSearch = sessionStorage.getItem("AppTypeSearch");

    if (teamName != null) {
        $('#appDetailSection').show();

        $('#btnNewServerRequest').show();
        $('#btnNewApplicationRequest').show();
        $('#btnDataChangeRequest').show();
        $('#btnRetireRequest').show();
        $('#requesterInfo').show(); 
        $('#allTeamsList :selected').text(teamName);
        $('#allEnvironmentsList').show();
        
        GetAdminUsersForTeam();

        switch (appTypeSearch) {
            case "Applications":
                $('#appTypeSearch').prop('checked', false);
                GenerateApplicationsTable(teamName);
                $('#allAppTypeList').show();
                break;
            case "Servers":
                $('#appTypeSearch').prop('checked', true);
                GenerateServersTable(teamName);
                $('#allAppTypeList').hide();
                break;
        }

    } else {
        $('#appDetailSection').hide();
    }
});

function GetAdminUsersForTeam() {
    var teamName = sessionStorage.getItem("TeamName");

    $.ajax({
        url: mainUrl + "Home/GetAdminUsersForTeam",
        type: 'GET',
        data: {
            teamName: teamName
        },
        dataType: "json",
        success: function (data) {
            sessionStorage.setItem("TeamAdminUsers", data.adminUsers);

            var user = $('#user')[0].innerText;

            if (data.adminUsers.includes(user)) {
                isAdmin = true;
            } else {
                isAdmin = false;            
            }
        }
    });
}

function GenerateApplicationsTable(selectedTeamName) {
    $('#appDetailSection').show();
    $('#serverDetailsSection').hide();
    // populate drop downs for app type and environments
   
    applicationsTable = $("#applicationResultsTable").DataTable({
        "dom": '<"top"l>prt<"bottom"ip><"clear">',
        "destroy": true,
        "bAutoWidth": true,
        "ajax": {
            "url": mainUrl + "Home/GetApplications",
            "error": function (errResp) {
                alert("There was an issue processing your request. Please try again.");
            },
            "data": {
                "teamName": selectedTeamName
            },
            "dataSrc": function (json) {
                var data = JSON.parse(json.data);
                return data;
            },
            "type": "GET",
            "datatype": "json"
        },
        "language": {
            "emptyTable": "There are currently no Applications to display."
        },
        "columns": [
            {
                "data": "",
                sDefaultContent: "",
                "render": function (name, type, data, meta) {
                    var server = data.ServerName;
                    var results = "";
                    if (server !== null) {
                        if (server.indexOf(";") > 0) {
                            var result = server.split(";");
                            $.each(result,
                                function (key, value) {
                                    var serverName = value.trim();

                                    if (serverName !== "") {
                                        var serverParams = encodeURIComponent(serverName) +
                                            "," +
                                            encodeURIComponent(data.TeamName);
                                        var serverlink = 'DisplayModal();DisplayServerDetails("' + serverParams + '")';
                                        results +=
                                            "<button id='serverDetails' class='btn btn-info' title='View Additional Details about this Server' onclick=" +
                                            serverlink + "><i class='fas fa-info-circle'></i> " + serverName + "</button><br><br>";

                                    }
                                });
                        }
                    }
                    var appParams = encodeURIComponent(data.AppName) +
                        "," +
                        data.AppEnvironment +
                        "," +
                        encodeURIComponent(data.TeamName) + "," +
                        encodeURIComponent(data.ServerName);

                    var requestType = 'Application Update';

                    var updateAppParams = encodeURIComponent(data.AppName) +
                        "," +
                        data.AppEnvironment +
                        "," +
                        encodeURIComponent(data.TeamName) + "," +
                        encodeURIComponent(data.ServerName) + "," + encodeURIComponent(requestType);

                    var appLink = 'DisplayModal();DisplayAppDetails("' + appParams + '")';

                    var editLink = 'DisplayModal();DisplayUpdateAppDetails("' + updateAppParams + '")';

                    results +=
                        "<button id='appDetails' class='btn btn-primary' title='View Additional Details about this Application' onclick=" +
                        appLink +
                        "><i class='fas fa-info-circle'></i> View App Details</button>";

                    if (isAdmin) {
                        results += "<br><br><button id='updateAppDetails' class='btn btn-warning' title='Update Details about this Application' onclick=" +
                            editLink +
                            "><i class='far fa-edit'></i> Update Details</button>";
                    }
                    return results;
                }
            },
            {
                "data": "AppName",
                sDefaultContent: "",
                "render": function (name, type, data, meta) {
                    var appOwner = "";
                    if (data.AppOwner === null) {
                        appOwner = "N/A";
                    } else {
                        appOwner = data.AppOwner;
                    }

                    return "<h5><i><u>" + data.AppName + "</u></i></h5><p><b>App Owner: </b><br>" + appOwner + "</p>" + "<p><b>App Environment: </b><br>" + data.AppEnvironment + "</p>";
                }
            },
            {
                "data": "AppStatus",
                sDefaultContent: "",
                "render": function (name, type, data, meta) {
                    return "<p><b>App Status: </b><br>" + data.AppStatus + "</p>" + "<p><b>Approval Status: </b><br>" + data.ApprovalStatus + "</p>";
                }
            },
            { "data": "AppType" },
            {
                "data": "AppUrl",
                sDefaultContent: "",
                "render": function (name, type, data, meta) {
                    if (data.AppUrl === null) {
                        return "N/A";
                    } else {
                        switch (data.AppType) {
                            case "File Directory":
                                return "<i class='far fa-folder-open fa-2x'></i>";
                            case "Import Process":
                                return "<i class='fas fa-file-import fa-2x'></i>";
                            case "API":
                                return "<i class='fas fa-file-import fa-2x'></i>";
                            case "Windows Service":
                                return "<i class='fab fa-windows fa-2x'></i>";
                            case "Desktop Application":
                                return "<i class='fas fa-desktop fa-2x'></i>";
                            case "Microservice":
                                return "<i class='fas fa-file-import fa-2x'></i>";
                            case "Mobile Application":
                                if (data.AppStatus === 'Active') {
                                    return "<i class='fas fa-mobile-alt fa-2x'></i><br><br><a id='appUrl' class='btn btn-light' target='_blank' title='View page to Download Mobile application for your device.' href='" + data.AppUrl + "'>Download Mobile App</a><br><br>";
                                } else {
                                    return "<i class='fas fa-mobile-alt fa-2x'></i>";
                                }
                            default:
                                if (data.AppStatus === 'Active') {
                                    if (data.AppUrl === "N/A") {
                                        return "<i class='fas fa-desktop fa-2x'></i>";
                                    } else {
                                        return "<i class='fas fa-desktop fa-2x'></i><br><br><a id='appUrl' class='btn btn-light' target='_blank' title='View Web Application' href='" +
                                            data.AppUrl +
                                            "'>View Web App</a><br><br>";
                                    }
                                } else {
                                    if (data.AppUrl === "N/A") {
                                        return "N/A";
                                    } else {
                                        return "<i class='fas fa-desktop fa-2x'></i>";
                                    }
                                }
                        }
                    }
                }
            }
        ],
        "columnDefs": [
            {
                targets: 0,
                width: "18%",
                className: 'text-center',
            },
            {
                targets: 1,
                className: 'text-center'
            },
            {
                targets: 2,
                className: 'text-center'
            },
            {
                targets: 3,
                className: 'text-center',
                width: "10%"
            },
            {
                targets: 4,
                className: 'text-center'
            }
        ],
        "pageLength": 10,
        "pagingType": "full_numbers",
        order: [[1, 'asc']],
        rowCallback: function (row, data, index) {
            switch (data.AppEnvironment) {
                case "Dev":
                    $(row).find('td:eq(1)').css('background-color', '#CCABDE');
                    break;
                case "QA":
                    $(row).find('td:eq(1)').css('background-color', '#B1C8F7');
                    break;
                case "Production":
                    $(row).find('td:eq(1)').css('background-color', '#66D589');
                    break;
            }

            switch (data.AppStatus) {
                case "Active":
                    $(row).find('td:eq(2)').css('background-color', '#32c532');
                    $(row).find('td:eq(4)').css('background-color', '#32c532');
                    break;
                case "Not Active":
                    $(row).find('td:eq(2)').css('background-color', '#ff0000');
                    $(row).find('td:eq(4)').css('background-color', '#ff0000');
                    break;
                case "On Decommission List":
                    $(row).find('td:eq(2)').css('background-color', '#FFBA69');
                    $(row).find('td:eq(4)').css('background-color', '#FFBA69');
                    break;
                case "N/A":
                    $(row).find('td:eq(2)').css('background-color', '');
                    break;
            }
        },
        responsive: true
    });
}

function GenerateServersTable(teamName) {

    $('#appDetailSection').hide();
    $('#serverDetailsSection').show();

    serversTable = $("#serverTable").DataTable({
        "dom": '<"top"l>prt<"bottom"ip><"clear">',
        "destroy": true,
        "bAutoWidth": true,
        "ajax": {
            "url": mainUrl + "Home/GetServersByTeam",
            "error": function (errResp) {
                alert("There was an issue processing your request. Please try again.");
            },
            "data": {
                "teamName": teamName
            },
            "dataSrc": function (json) {
                var data = JSON.parse(json.data);
                return data;
            },
            "type": "GET",
            "datatype": "json"
        },
        "language": {
            "emptyTable": "There are currently no Servers to display."
        },
        "columns": [
            {
                "data": "",
                sDefaultContent: "",
                "render": function (name, type, data, meta) {
                    var results = "";

                    var serverName = data.ServerName.trim();

                    var requestType = 'Server Update';

                    var serverParams = encodeURIComponent(serverName) +
                        "," +
                        encodeURIComponent(data.TeamName);

                    var updateServerParams = encodeURIComponent(serverName) +
                        "," +
                        encodeURIComponent(data.TeamName) + "," + encodeURIComponent(requestType);
                
                    var serverlink = 'DisplayModal();DisplayServerDetails("' + serverParams + '")';

                    var editLink = 'DisplayModal();DisplayUpdateServerDetails("' + updateServerParams + '")';

                    results += "<button id='serverDetails' class='btn btn-info' title='View Additional Details about this Server' onclick=" +
                        serverlink + "><i class='fas fa-info-circle'></i> Server Details</button>";

                    if (isAdmin) {
                        results += "<br><br><button id='updateServerDetails' class='btn btn-warning' title='Update Details about this Server' onclick=" +
                            editLink +
                            "><i class='far fa-edit'></i> Update Details</button>";
                    }
                    return results;
                }
            },
            {
                "data": "ServerName",
                sDefaultContent: "",
                "render": function (name, type, data, meta) {

                    return "<h5><i><u>" + data.ServerName + "</u></i></h5><p><b>Operating System: </b><br>" + data.OperatingSystem + "</p><p><b>App Environment: </b><br>" + data.Environment + "</p>";
                }
            },
            { "data": "TeamName" },
            {
                "data": "ServerStatus",
                sDefaultContent: "",
                "render": function (name, type, data, meta) {
                    return "<p><b>Server Status: </b><br>" + data.ServerStatus + "</p>" + "<p><b>Approval Status: </b><br>" + data.ApprovalStatus + "</p>";
                }
            },
            {
                "data": "InAltirisForPatching",
                sDefaultContent: "",
                "render": function (name, type, data, meta) {
                    switch (data.InAltirisForPatching) {
                        case "True":
                            return '<i class="far fa-check-circle fa-2x" style="color: green !important;"></i>';
                        case "False":
                            return '<i class="far fa-times-circle fa-2x" style="color: red !important;"></i>';
                    }
                    return "<h5><i><u>" + data.ServerName + "</u></i></h5><p><b>App Environment: </b><br>" + data.Environment + "</p>";
                }
            },
            { "data": "AltirisPatchingGroup" }
        ],
        "columnDefs": [
            {
                targets: 0,
                className: 'text-center',
                width: "17%"
            },
            {
                targets: 1,
                className: 'text-center'
            },
            {
                targets: 2,
                className: 'text-center'
            },
            {
                targets: 3,
                className: 'text-center'
            },
            {
                targets: 4,
                className: 'text-center'
            },
            {
                targets: 5,
                className: 'text-center'
            }
        ],
        "pageLength": 10,
        "pagingType": "full_numbers",
        order: [[0, 'asc']],
        rowCallback: function (row, data, index) {
            switch (data.Environment) {
                case "Dev":
                    $(row).find('td:eq(1)').css('background-color', '#CCABDE');
                    break;
                case "QA":
                    $(row).find('td:eq(1)').css('background-color', '#B1C8F7');
                    break;
                case "Production":
                    $(row).find('td:eq(1)').css('background-color', '#66D589');
                    break;
            }

            switch (data.ServerStatus) {
                case "Active":
                    $(row).find('td:eq(3)').css('background-color', '#32c532');
                    break;
                case "Not Active":
                    $(row).find('td:eq(3)').css('background-color', '#ff0000');
                    break;
                case "On Decommission List":
                    $(row).find('td:eq(3)').css('background-color', '#FFBA69');
                    break;
                case "N/A":
                    $(row).find('td:eq(3)').css('background-color', '');
                    break;
            }
        },
        responsive: true
    });
}

function DisplayServerDetails(params) {
    $.ajax({
        url: mainUrl + "Home/GetServerDetails",
        type: 'GET',
        data: {
            searchParams: params
        },
        dataType: "json",
        success: function (data) {
            if (data != null) {
                $('#modaltitle')[0].innerText = "Server Details for: " + data[0].detailValue;

                $("#loadingModal").modal("hide");

                $("#additionalDetails").modal("show");

                var mycolumns = [];
                $.each(data[0], function (key, value) {
                    var my_item = {};
                    my_item.data = key;
                    my_item.title = key;
                    mycolumns.push(my_item);
                });

                $('#addlDetailsTable').DataTable({
                    data: data,
                    "language": {
                        "emptyTable": "There are currently no Details to display for this server."
                    },
                    "destroy": true,
                    "columns": mycolumns,
                    "bPaginate": false
                });
            }
        }
    });
}

function DisplayAppDetails(params) {
    $.ajax({
        url: mainUrl + "Home/GetAdditionalAppDetails",
        type: 'GET',
        data: {
            searchParams: params
        },
        dataType: "json",
        success: function (data) {
            if (data != null) {

                $('#modaltitle')[0].innerText = "Additional App Details for: " + data[0].detailValue;

                $("#loadingModal").modal("hide");

                $("#additionalDetails").modal("show");

                var mycolumns = [];
                $.each(data[0], function (key, value) {
                    var my_item = {};
                    my_item.data = key;
                    my_item.title = key;
                    mycolumns.push(my_item);
                });

                $('#addlDetailsTable').DataTable({
                    data: data,
                    "language": {
                        "emptyTable": "There are currently no Details to display for this application."
                    },
                    "destroy": true,
                    "columns": mycolumns,
                    "bPaginate": false
                });
            }
        }
    });
}

function NewServerRequest(server) {

    var teamName = sessionStorage.getItem("TeamName");

    $('#newRequestAdminUsers').val(sessionStorage.getItem("TeamAdminUsers"));

    $.ajax({
        url: mainUrl + "NewRequest/GetNewRequestHeaderInfo",
        type: 'GET',
        data: {
            headerType: server,
            teamName: teamName
        },
        dataType: "json",
        success: function (data) {
            if (data != null) {

                $('#modaltitle1')[0].innerText = "New Server Request Details for " + teamName;

                $('#submitBtn').hide();
                $('#requesterInfo').show();

                $("#loadingModal").modal("hide");

                $("#newRequest").modal("show");

                $('#newRequestDetails').html('');
                var newHtml = '';
                for (var i = 0; i < data.length; i++) {
                    //var notAppliedLink = 'NotApplicable("' + data[i].headerName + '_Checkbox")';

                    newHtml += '<div class="col-md-4"><br>';
                    if (data[i].notesAboutField !== "") {

                        var noteDetails = encodeURIComponent(data[i].headerDisplayName) + "," + encodeURIComponent(data[i].notesAboutField);
                        var noteLink = 'PopulateFieldNotes("' + noteDetails + '")';

                        switch (data[i].headerDisplayName) {
                            case "Server Name":
                                newHtml += "<label><b>" + data[i].headerDisplayName + "<span style='color:red !important;'>*</span></b></label> <a href='' data-toggle='modal' data-target='#notesModal' onclick=" + noteLink + "><i class='fas fa-info-circle'></i></a><br>";
                                break;
                            case "Operating System":
                                newHtml += "<label><b>" + data[i].headerDisplayName + "<span style='color:red !important;'>*</span></b></label> <a href='' data-toggle='modal' data-target='#notesModal' onclick=" + noteLink + "><i class='fas fa-info-circle'></i></a><br>";
                                break;
                            default:
                                newHtml += "<label><b>" + data[i].headerDisplayName + "</b></label> <a href='' data-toggle='modal' data-target='#notesModal' onclick=" + noteLink + "><i class='fas fa-info-circle'></i></a><br>";
                                break;
                        }

                    } else {
                        if (data[i].headerDisplayName !== "Id") {
                            newHtml += '<label><b>' + data[i].headerDisplayName + '</b></label><br>';
                        }
                    }

                    switch (data[i].type) {
                        case "FieldUserValue[]":
                            newHtml += '<textarea class="form-control" id="' + data[i].headerName + '_input" rows="2" cols="60" name="' + data[i].headerName + '"></textarea>';
                            newHtml += '<small>Please enter AD Usernames ONLY separated using a comma.</small><br>';

                            /*var notAppliedLink = 'NotApplicable("' + data[i].headerName + '_Checkbox")';*/
                            /* newHtml += '<input type="checkbox" id="' + data[i].headerName + '_Checkbox" onclick=' + notAppliedLink + '><span> <i> This field doesnt apply to this request.</i></span>';*/
                            break;

                        case "String":
                            switch (data[i].headerDisplayName) {
                                case "Team Name":
                                    newHtml += '<textarea class="form-control" readonly="readonly" rows="1" cols="60" name="' + data[i].headerName + '">' + teamName + '</textarea>';
                                    break;
                                case "Server Name":
                                    /* newHtml += '<textarea class="form-control" id="' + data[i].headerName + '_input" required="required" rows="1" cols="60" name="' + data[i].headerName + '"></textarea>';*/
                                    newHtml += '<input type="text" class="form-control" id="' + data[i].headerName + '_input" required="required" name="' + data[i].headerName + '">';
                                    break;
                                case "Operating System":
                                    /* newHtml += '<textarea class="form-control" id="' + data[i].headerName + '_input" required="required" rows="1" cols="60" name="' + data[i].headerName + '"></textarea>';*/
                                    newHtml += '<input type="text" class="form-control" id="' + data[i].headerName + '_input" required="required" name="' + data[i].headerName + '">';
                                    /*   newHtml += '<input type="checkbox" id="' + data[i].headerName + '_Checkbox" onclick=' + notAppliedLink + '><span><i> No data to input.</i></span>';*/
                                    break;
                                default:
                                    newHtml += '<textarea class="form-control" id="' + data[i].headerName + '_input" rows="2" cols="60" name="' + data[i].headerName + '"></textarea>';
                                    /*newHtml += '<input type="checkbox" id="' + data[i].headerName + '_Checkbox" onclick=' + notAppliedLink + '><span><i> No data to input.</i></span>';*/
                                    break;
                            }
                            //if (data[i].headerDisplayName === 'Team Name') {
                            //    newHtml += '<textarea class="form-control" readonly="readonly" rows="1" cols="60" name="' + data[i].headerName + '">' + teamName + '</textarea>';
                            //   /* newHtml += '<input type="text" class="form-control" readonly="readonly" name="' + data[i].headerName + '" value=\\' + teamName + '>';*/
                            //} else {
                            //    newHtml += '<textarea class="form-control" id="' + data[i].headerName + '_input" required="required" rows="2" cols="60" name="' + data[i].headerName + '"></textarea>';

                            //    //var notAppliedLink = 'NotApplicable("' + data[i].headerName + '_Checkbox")';
                            //    //newHtml += '<input type="checkbox" id="' + data[i].headerName + '_Checkbox" onclick=' + notAppliedLink + '><span><i> This field doesnt apply to this request.</i></span>';
                            //}
                            break;
                        case "FieldLookupValue":
                            newHtml += '<select class="form-control" id="' + data[i].headerName + '_input" name="' + data[i].headerName + '">';
                            newHtml += '<option disabled="disabled" selected="selected" value="0">Please Select an option...</option>';
                            for (var j = 0; j < data[i].options.length; j++) {
                                newHtml += '<option value="' + data[i].options[j].dropDownOption + '">' + data[i].options[j].dropDownOption + '</option>';
                            }

                            newHtml += '</select>';
                            break;

                        case "FieldLookupValue[]":
                            newHtml += '<div class="fieldLookupList">';
                            newHtml += '<table>';
                            newHtml += '<tbody>';
                            for (var x = 0; x < data[i].options.length; x++) {
                                newHtml += '<tr>';
                                newHtml += '<td>';
                                newHtml += '<input type="checkbox" value="' + data[i].options[x].dropDownOption + '" name="' + data[i].headerName + '">';
                                newHtml += '</td>';

                                newHtml += '<td>';
                                newHtml += ' <label style="margin-bottom: 0!important; padding-left: 5%!important;" for="' + data[i].options[x].dropDownOption + '">' + data[i].options[x].dropDownOption + '</label>';
                                newHtml += '</td>';
                                newHtml += '</tr>';
                            }
                            newHtml += '</tbody>';
                            newHtml += '</table>';
                            newHtml += '</div>';
                            break;
                        case "Boolean":
                            newHtml += '<label class="switch">';
                            newHtml += '<input type="checkbox" id="' + data[i].headerName + '_check" name="' + data[i].headerName + '_check" onchange = "PopulateBoolField(this)">';
                            newHtml += '<span class="slider round"></span>';
                            newHtml += '</label >';
                            newHtml += '<input type="hidden" id="' + data[i].headerName + '_Result" name="' + data[i].headerName + '_Result" value="No" />';

                            //newHtml += '<select class="form-control" id="' + data[i].headerName + '_input" name="' + data[i].headerName + '">';
                            //newHtml += '<option disabled="disabled" selected="selected" value="0">Please Select an option...</option>';
                            //newHtml += '<option value="Yes">Yes</option>';
                            //newHtml += '<option value="No">No</option>';
                            //newHtml += '</select>';
                            break;
                    }
                    newHtml += '</div>';
                }
                newHtml += '<br><br><input type="hidden" class="form-control" name="RequestType" value="Server Request">';

                $('#newRequestDetails').append(newHtml);

                $('#submitBtn').show();
            } else {

                $('#requesterInfo').hide();
                $('#modaltitle1')[0].innerText = "New Server Request Details for " + teamName;

                $('#submitBtn').hide();

                $("#loadingModal").modal("hide");

                $("#newRequest").modal("show");

                $('#newRequestDetails').html('');

                var newHtml = '<div class="col-md-12 text-center"><h3 style="color: red !important;">There was an issue retrieving the Server Details Request form.<h3></div>';

                $('#newRequestDetails').append(newHtml);
            }
        }
    });
}

function NewApplicationRequest(application) {
    var teamName = sessionStorage.getItem("TeamName");

    $('#newRequestAdminUsers').val(sessionStorage.getItem("TeamAdminUsers"));

    $.ajax({
        url: mainUrl + "NewRequest/GetNewRequestHeaderInfo",
        type: 'GET',
        data: {
            headerType: application,
            teamName: teamName
        },
        dataType: "json",
        success: function (data) {
            if (data != null) {

                $('#modaltitle1')[0].innerText = "New Application Request Details for " + teamName;

                $('#submitBtn').hide();
                $('#requesterInfo').show();

                $("#loadingModal").modal("hide");

                $("#newRequest").modal("show");

                $('#newRequestDetails').html('');

                var newHtml = '';
                for (var i = 0; i < data.length; i++) {
                    newHtml += '<div class="col-md-4"><br>';
                    if (data[i].notesAboutField !== "") {

                        var noteDetails = encodeURIComponent(data[i].headerDisplayName) + "," + encodeURIComponent(data[i].notesAboutField);
                        var noteLink = 'PopulateFieldNotes("' + noteDetails + '")';

                        switch (data[i].headerDisplayName) {
                            case "App Name":
                                newHtml += "<label><b>" + data[i].headerDisplayName + "<span style='color:red !important;'>*</span></b></label> <a href='' data-toggle='modal' data-target='#notesModal' onclick=" + noteLink + "><i class='fas fa-info-circle'></i></a><br>";
                                break;
                            default:
                                newHtml += "<label><b>" + data[i].headerDisplayName + "</b></label> <a href='' data-toggle='modal' data-target='#notesModal' onclick=" + noteLink + "><i class='fas fa-info-circle'></i></a><br>";
                                break;
                        }

                    } else {
                        if (data[i].headerDisplayName !== "Id") {
                            newHtml += '<label><b>' + data[i].headerDisplayName + '</b></label><br>';
                        }                       
                    }

                    switch (data[i].type) {
                        case "FieldUserValue[]":
                            if (data[i].headerDisplayName === 'Team Name') {
                                newHtml += '<textarea class="form-control" readonly="readonly" rows="2" cols="60" name="' + data[i].headerName + '">' + teamName + '</textarea>';
                            } else {
                                newHtml += '<textarea class="form-control" id="' + data[i].headerName + '_input" rows="2" cols="60" name="' + data[i].headerName + '"></textarea>';
                                newHtml += '<small>Please enter AD Usernames ONLY separated using a comma.</small><br>';

                                //var notAppliedLink = 'NotApplicable("' + data[i].headerName + '_Checkbox")';
                                //newHtml += '<input type="checkbox" id="' + data[i].headerName + '_Checkbox" onclick=' + notAppliedLink + '><span> <i> This field doesnt apply to this request.</i></span>';
                            }
                            break;
                        case "String":
                            switch (data[i].headerDisplayName) {
                                case "Team Name":
                                    newHtml += '<textarea class="form-control" readonly="readonly" rows="1" cols="60" name="' + data[i].headerName + '">' + teamName + '</textarea>'; break;
                                case "App Name":
                                    /* newHtml += '<textarea class="form-control" id="' + data[i].headerName + '_input" required="required" rows="1" cols="60" name="' + data[i].headerName + '"></textarea>';*/
                                    newHtml += '<input type="text" class="form-control" id="' + data[i].headerName + '_input" required="required" name="' + data[i].headerName + '">';
                                    break;
                                default:
                                    newHtml += '<textarea class="form-control" id="' + data[i].headerName + '_input" rows="2" cols="60" name="' + data[i].headerName + '"></textarea>';
                                    break;
                            }
                            //if (data[i].headerDisplayName === 'Team Name') {
                            //    newHtml += '<textarea class="form-control" readonly="readonly" rows="2" cols="60" name="' + data[i].headerName + '">' + teamName + '</textarea>';
                            //} else {
                            //    newHtml += '<textarea class="form-control" id="' + data[i].headerName + '_input" required="required" rows="2" cols="60" name="' + data[i].headerName + '"></textarea>';

                            //    var notAppliedLink = 'NotApplicable("' + data[i].headerName + '_Checkbox")';
                            //    newHtml += '<input type="checkbox" id="' + data[i].headerName + '_Checkbox" onclick=' + notAppliedLink + '><span> <i> This field doesnt apply to this request.</i></span>';
                            //}
                            break;
                        case "FieldLookupValue":
                            newHtml += '<select class="form-control" id="' + data[i].headerName + '_input" name="' + data[i].headerName + '">';
                            newHtml += '<option disabled="disabled" selected="selected" value="0">Please Select an option...</option>';
                            for (var j = 0; j < data[i].options.length; j++) {
                                newHtml += '<option value="' + data[i].options[j].dropDownOption + '">' + data[i].options[j].dropDownOption + '</option>';
                            }

                            newHtml += '</select>';
                            break;

                        case "FieldLookupValue[]":
                            //newHtml += '<input type="text" class="form-control" id="' + data[i].headerName + '_Myinput" onkeyup ="myFunction(this)" placeholder="Search..">'

                            newHtml += '<div class="fieldLookupList">';
                            newHtml += '<table id="' + data[i].headerName + '_table">';
                            newHtml += '<tbody>';
                            for (var x = 0; x < data[i].options.length; x++) {
                                newHtml += '<tr>';
                                newHtml += '<td>';
                                newHtml += '<input type="checkbox" value="' + data[i].options[x].dropDownOption + '" name="' + data[i].headerName + '">';
                                newHtml += '</td>';

                                newHtml += '<td>';
                                newHtml += ' <label style="margin-bottom: 0!important; padding-left: 5%!important;" for="' + data[i].options[x].dropDownOption + '">' + data[i].options[x].dropDownOption + '</label>';
                                newHtml += '</td>';
                                newHtml += '</tr>';
                            }
                            newHtml += '</tbody>';
                            newHtml += '</table>';
                            newHtml += '</div>';
                            break;
                        case "Boolean":
                            newHtml += '<label class="switch">';
                            newHtml += '<input type="checkbox" id="' + data[i].headerName + '_check" name="' + data[i].headerName + '_check" onchange = "PopulateBoolField(this)">';
                            newHtml += '<span class="slider round"></span>';
                            newHtml += '</label >';
                            newHtml += '<input type="hidden" id="' + data[i].headerName + '_Result" name="' + data[i].headerName + '_Result" value="No" />';

                            //newHtml += '<select class="form-control" id="' + data[i].headerName + '_input" name="' + data[i].headerName + '">';
                            //newHtml += '<option disabled="disabled" selected="selected" value="0">Please Select an option...</option>';
                            //newHtml += '<option value="Yes">Yes</option>';
                            //newHtml += '<option value="No">No</option>';
                            //newHtml += '</select>';
                            break;
                    }
                    newHtml += '</div>';
                }
                newHtml += '<br><br><input type="hidden" class="form-control" name="RequestType" value="Application Request">';

                $('#newRequestDetails').append(newHtml);

                $('#submitBtn').show();
            } else {
                $('#modaltitle1')[0].innerText = "New Application Request Details for " + teamName;

                $('#submitBtn').hide();
                $('#requesterInfo').hide();

                $("#loadingModal").modal("hide");

                $("#newRequest").modal("show");

                $('#newRequestDetails').html('');

                var newHtml = '<div class="col-md-12 text-center"><h3 style="color: red !important;">There was an issue retrieving the Application Details Request form.<h3></div>';

                $('#newRequestDetails').append(newHtml);
            }
        }
    });
}

function NotApplicable(value) {
    var result = '#' + value;

    var newResult = value.replace('_Checkbox', '_input');
    var field = '#' + newResult;

    if ($(result).prop('checked') === true) {
        $(field).val("N/A");
        $(field).prop("readonly", true);
    } else {
        $(field).val("");
        $(field).prop("readonly", false);
    }
}

function PopulateFieldNotes(noteDetails) {

    $("#noteDetails").modal("show");

    var res = noteDetails.split(",");
    var header = decodeURIComponent(res[0]);

    var note = decodeURIComponent(res[1]);

    $('#noteTitle')[0].innerText = header;
    $('#noteDetails')[0].innerText = note;
}

function DisplayUpdateAppDetails(params) {
    var teamName = sessionStorage.getItem("TeamName");

    $('#newUpdateAdminUsers').val(sessionStorage.getItem("TeamAdminUsers"));

    $.ajax({
        url: mainUrl + "Home/GetAppDetailsForUpdate",
        type: 'GET',
        data: {
            searchParams: params
        },
        dataType: "json",
        success: function (dataResponse) {
            if (dataResponse != null) {
                response = dataResponse;

                $('#editRequesterInfo').show();

                $("#loadingModal").modal("hide");
                $("#editAdditionalDetails").modal("show");

                $('#editModaltitle')[0].innerText = "Update/Modify App Details for: " + dataResponse[1].existingValue;

                $('#editAddlDetails').html('');

                var newHtml = '';

                for (var i = 0; i < dataResponse.length; i++) {
                    newHtml += '<div class="col-md-4"><br>';
                    newHtml += '<label><b>' + dataResponse[i].headerDisplayName + '</b></label><br>';

                    switch (dataResponse[i].type) {
                        case "FieldUserValue[]":
                            if (dataResponse[i].headerDisplayName === 'Team Name') {
                                newHtml += '<textarea class="form-control" readonly="readonly" rows="2" cols="60" name="' + dataResponse[i].headerName + '">' + teamName + '</textarea>';
                            } else {
                                newHtml += '<textarea class="form-control" id="' + dataResponse[i].headerName + '_input" rows="2" cols="60" name="' + dataResponse[i].headerName + '">' + dataResponse[i].existingValue + '</textarea>';
                                newHtml += '<small>Please enter AD Usernames ONLY separated using a comma.</small><br>';                               
                            }
                            break;
                        case "String":
                            switch (dataResponse[i].headerDisplayName) {
                                case "Team Name":
                                    newHtml += '<textarea class="form-control" readonly="readonly" rows="1" cols="60" name="' + dataResponse[i].headerName + '">' + teamName + '</textarea>'; break;
                                case "App Name":
                                    newHtml += '<input type="text" class="form-control" id="' + dataResponse[i].headerName + '_input" readonly="readonly" name="' + dataResponse[i].headerName + '" value="' + dataResponse[i].existingValue + '">';
                                    break;
                                case "Notes about App":
                                    newHtml += '<textarea class="form-control" id="' + dataResponse[i].headerName + '_input" rows="6" cols="60" name="' + dataResponse[i].headerName + '">' + dataResponse[i].existingValue + '</textarea>';
                                    break;
                                default:
                                    newHtml += '<textarea class="form-control" id="' + dataResponse[i].headerName + '_input" rows="2" cols="60" name="' + dataResponse[i].headerName + '">' + dataResponse[i].existingValue + '</textarea>';
                                    break;
                            }
                            break;
                        case "Int32":
                            newHtml += '<input type="text" class="form-control" id="' + dataResponse[i].headerName + '_input" readonly="readonly" name="' + dataResponse[i].headerName + '" value="' + dataResponse[i].existingValue + '">';
                            break;
                        case "FieldLookupValue":
                            newHtml += '<select class="form-control" id="' + dataResponse[i].headerName + '_input" name="' + dataResponse[i].headerName + '">';
                            newHtml += '<option disabled="disabled" value="0">Please Select an option...</option>';
                            for (var j = 0; j < dataResponse[i].options.length; j++) {
                                if (dataResponse[i].existingValue === dataResponse[i].options[j].dropDownOption) {
                                    newHtml += '<option selected="selected" value="' + dataResponse[i].options[j].dropDownOption + '">' + dataResponse[i].options[j].dropDownOption + '</option>';
                                }
                                else {
                                    newHtml += '<option value="' + dataResponse[i].options[j].dropDownOption + '">' + dataResponse[i].options[j].dropDownOption + '</option>';
                                }
                            }

                            newHtml += '</select>';
                            break;

                        case "FieldLookupValue[]":
                            newHtml += '<div class="fieldLookupList">';
                            newHtml += '<table id="' + dataResponse[i].headerName + '_table">';
                            newHtml += '<tbody>';
                            for (var x = 0; x < dataResponse[i].options.length; x++) {
                                newHtml += '<tr>';
                                newHtml += '<td>';
                                if (dataResponse[i].existingValue !== null) {
                                    if (dataResponse[i].existingValue.includes(dataResponse[i].options[x].dropDownOption)) {
                                        newHtml += '<input type="checkbox" checked="checked" value="' + dataResponse[i].options[x].dropDownOption + '" name="' + dataResponse[i].headerName + '">';
                                    }
                                    else {
                                        newHtml += '<input type="checkbox" value="' + dataResponse[i].options[x].dropDownOption + '" name="' + dataResponse[i].headerName + '">';
                                    }
                                }
                                else {
                                    newHtml += '<input type="checkbox" value="' + dataResponse[i].options[x].dropDownOption + '" name="' + dataResponse[i].headerName + '">';
                                }
                                newHtml += '</td>';

                                newHtml += '<td>';
                                newHtml += ' <label style="margin-bottom: 0!important; padding-left: 5%!important;" for="' + dataResponse[i].options[x].dropDownOption + '">' + dataResponse[i].options[x].dropDownOption + '</label>';
                                newHtml += '</td>';
                                newHtml += '</tr>';
                            }
                            newHtml += '</tbody>';
                            newHtml += '</table>';
                            newHtml += '</div>';
                            break;
                        case "Boolean":
                            newHtml += '<label class="switch">';
                            if (dataResponse[i].existingValue === "True") {
                                newHtml += '<input type="checkbox" checked="checked" id="' + dataResponse[i].headerName + '_check" name="' + dataResponse[i].headerName + '_check" onchange = "PopulateBoolField(this)">';
                            } else {
                                newHtml += '<input type="checkbox" id="' + dataResponse[i].headerName + '_check" name="' + dataResponse[i].headerName + '_check" onchange = "PopulateBoolField(this)">';
                            }

                            newHtml += '<span class="slider round"></span>';
                            newHtml += '</label >';
                            newHtml += '<input type="hidden" id="' + dataResponse[i].headerName + '_Result" name="' + dataResponse[i].headerName + '_Result" value="No" />';

                            break;
                    }
                    newHtml += '</div>';
                }

                newHtml += '<br><br><input type="hidden" class="form-control" name="RequestType" value="Application Update">';

                $('#editAddlDetails').append(newHtml);
            }
        }
    });
}

function DisplayUpdateServerDetails(params) {
    var teamName = sessionStorage.getItem("TeamName");

    $('#newUpdateAdminUsers').val(sessionStorage.getItem("TeamAdminUsers"));

    $.ajax({
        url: mainUrl + "Home/GetServerDetailsForUpdate",
        type: 'GET',
        data: {
            searchParams: params
        },
        dataType: "json",
        success: function (dataResponse) {
            if (dataResponse != null) {
                response = dataResponse;

                $('#editRequesterInfo').show();

                $("#loadingModal").modal("hide");
                $("#editAdditionalDetails").modal("show");

                $('#editModaltitle')[0].innerText = "Update/Modify Server Details for: " + dataResponse[1].existingValue;

                $('#editAddlDetails').html('');

                var newHtml = '';

                for (var i = 0; i < dataResponse.length; i++) {
                    newHtml += '<div class="col-md-4"><br>';
                    newHtml += '<label><b>' + dataResponse[i].headerDisplayName + '</b></label><br>';

                    switch (dataResponse[i].type) {
                        case "FieldUserValue[]":
                            if (dataResponse[i].headerDisplayName === 'Team Name') {
                                newHtml += '<textarea class="form-control" readonly="readonly" rows="2" cols="60" name="' + dataResponse[i].headerName + '">' + teamName + '</textarea>';
                            } else {
                                newHtml += '<textarea class="form-control" id="' + dataResponse[i].headerName + '_input" rows="2" cols="60" name="' + dataResponse[i].headerName + '">' + dataResponse[i].existingValue + '</textarea>';
                                newHtml += '<small>Please enter AD Usernames ONLY separated using a comma.</small><br>';
                            }
                            break;
                        case "String":
                            switch (dataResponse[i].headerDisplayName) {
                                case "Team Name":
                                    newHtml += '<textarea class="form-control" readonly="readonly" rows="1" cols="60" name="' + dataResponse[i].headerName + '">' + teamName + '</textarea>'; break;
                                case "Server Name":
                                    newHtml += '<input type="text" class="form-control" id="' + dataResponse[i].headerName + '_input" readonly="readonly" name="' + dataResponse[i].headerName + '" value="' + dataResponse[i].existingValue + '">';
                                    break;
                                case "Notes about App":
                                    newHtml += '<textarea class="form-control" id="' + dataResponse[i].headerName + '_input" rows="6" cols="60" name="' + dataResponse[i].headerName + '">' + dataResponse[i].existingValue + '</textarea>';
                                    break;
                                default:
                                    newHtml += '<textarea class="form-control" id="' + dataResponse[i].headerName + '_input" rows="2" cols="60" name="' + dataResponse[i].headerName + '">' + dataResponse[i].existingValue + '</textarea>';
                                    break;
                            }
                            break;

                        case "Int32":
                            newHtml += '<input type="text" class="form-control" id="' + dataResponse[i].headerName + '_input" readonly="readonly" name="' + dataResponse[i].headerName + '" value="' + dataResponse[i].existingValue + '">';
                            break;
                        case "FieldLookupValue":
                            newHtml += '<select class="form-control" id="' + dataResponse[i].headerName + '_input" name="' + dataResponse[i].headerName + '">';
                            newHtml += '<option disabled="disabled" value="0">Please Select an option...</option>';
                            for (var j = 0; j < dataResponse[i].options.length; j++) {
                                if (dataResponse[i].existingValue === dataResponse[i].options[j].dropDownOption) {
                                    newHtml += '<option selected="selected" value="' + dataResponse[i].options[j].dropDownOption + '">' + dataResponse[i].options[j].dropDownOption + '</option>';
                                }
                                else {
                                    newHtml += '<option value="' + dataResponse[i].options[j].dropDownOption + '">' + dataResponse[i].options[j].dropDownOption + '</option>';
                                }
                            }

                            newHtml += '</select>';
                            break;

                        case "FieldLookupValue[]":
                            newHtml += '<div class="fieldLookupList">';
                            newHtml += '<table id="' + dataResponse[i].headerName + '_table">';
                            newHtml += '<tbody>';
                            for (var x = 0; x < dataResponse[i].options.length; x++) {
                                newHtml += '<tr>';
                                newHtml += '<td>';
                                if (dataResponse[i].existingValue !== null) {
                                    if (dataResponse[i].existingValue.includes(dataResponse[i].options[x].dropDownOption)) {
                                        newHtml += '<input type="checkbox" checked="checked" value="' + dataResponse[i].options[x].dropDownOption + '" name="' + dataResponse[i].headerName + '">';
                                    }
                                    else {
                                        newHtml += '<input type="checkbox" value="' + dataResponse[i].options[x].dropDownOption + '" name="' + dataResponse[i].headerName + '">';
                                    }
                                }
                                else {
                                    newHtml += '<input type="checkbox" value="' + dataResponse[i].options[x].dropDownOption + '" name="' + dataResponse[i].headerName + '">';
                                }
                                newHtml += '</td>';

                                newHtml += '<td>';
                                newHtml += ' <label style="margin-bottom: 0!important; padding-left: 5%!important;" for="' + dataResponse[i].options[x].dropDownOption + '">' + dataResponse[i].options[x].dropDownOption + '</label>';
                                newHtml += '</td>';
                                newHtml += '</tr>';
                            }
                            newHtml += '</tbody>';
                            newHtml += '</table>';
                            newHtml += '</div>';
                            break;
                        case "Boolean":
                            newHtml += '<label class="switch">';
                            if (dataResponse[i].existingValue === "True") {
                                newHtml += '<input type="checkbox" checked="checked" id="' + dataResponse[i].headerName + '_check" name="' + dataResponse[i].headerName + '_check" onchange = "PopulateBoolField(this)">';
                            } else {
                                newHtml += '<input type="checkbox" id="' + dataResponse[i].headerName + '_check" name="' + dataResponse[i].headerName + '_check" onchange = "PopulateBoolField(this)">';
                            }

                            newHtml += '<span class="slider round"></span>';
                            newHtml += '</label >';
                            newHtml += '<input type="hidden" id="' + dataResponse[i].headerName + '_Result" name="' + dataResponse[i].headerName + '_Result" value="No" />';

                            break;
                    }
                    newHtml += '</div>';
                }

                newHtml += '<br><br><input type="hidden" class="form-control" name="RequestType" value="Server Update">';

                $('#editAddlDetails').append(newHtml);
            }
        }
    });
}

function DisplayModal() {
    $("#loadingModal").modal("show");
}

function PopulateBoolField(element) {
    var results = element.id.replace('_check', '_Result');

    var id = '#' + results;

    if ($(element).is(":checked")) {
        var returnVal = true;
        $(element).attr("checked", returnVal);

        $(id).val("Yes");
    } else {
        $(id).val("No");
    }
}
$('#clearfilterButton').click(function () {
    var appTypeSearch = sessionStorage.getItem("AppTypeSearch");
    switch (appTypeSearch) {
        case "Applications":
            applicationsTable.search('').columns().search('').draw();
            $("#allAppTypeList").val('');
            selectedAppType = '';
            $("#allEnvironmentsList").val('');
            selectedEnvironment = '';
            $("#searchBox").val('');
            break;
        case "Servers":
            serversTable.search('').columns().search('').draw();
            $("#allEnvironmentsServerList").val('');
            selectedEnvironmentServer = '';
            $("#searchBoxServers").val('');
            break;
    }
});
$('#allTeamsList').change(function () {
    var selectedTeamName = $('#allTeamsList :selected').text();
    sessionStorage.setItem("TeamName", selectedTeamName);

    var teamName = sessionStorage.getItem("TeamName");

    $('#btnNewServerRequest').show();
    $('#btnNewApplicationRequest').show();
    $('#btnDataChangeRequest').show();
    $('#btnRetireRequest').show();
    $('#allEnvironmentsList').show();
    $('#allAppTypeList').show();
    $('#requesterInfo').show();

    GetAdminUsersForTeam();

    if ($('#appTypeSearch').prop('checked') === true) {
        sessionStorage.setItem("AppTypeSearch", "Servers");
        GenerateServersTable(teamName);
    } else {
        sessionStorage.setItem("AppTypeSearch", "Applications");
        GenerateApplicationsTable(teamName);
    }
});
$('#allEnvironmentsList').change(function () {
    var searchBox = $('#searchBox').val();
    applicationsTable.rows().search('').draw();
    selectedEnvironment = $('#allEnvironmentsList :selected').text();
    selectedAppType = $('#allAppTypeList :selected').text();
    var searchFilter = selectedEnvironment + " " + searchBox;
    if (!isNullOrEmpty(selectedAppType) && !isNullOrEmpty(searchBox)) {
        applicationsTable.column(1).search(searchFilter).column(3).search(selectedAppType).draw();
    }
    else if(!isNullOrEmpty(selectedAppType)&&isNullOrEmpty(searchBox))
    {
        applicationsTable.column(1).search(selectedEnvironment).column(3).search(selectedAppType).draw();
    }
    else if(isNullOrEmpty(selectedAppType)&&!isNullOrEmpty(searchBox))
    {
        applicationsTable.column(1).search(searchFilter).draw();
    }
    else {
        applicationsTable.rows().search(selectedEnvironment).draw();
    }
});
$("#allEnvironmentsServerList").change(function () {
    var searchBox = $('#searchBoxServers').val();
    serversTable.rows().search('').draw();
    selectedEnvironmentServer = $('#allEnvironmentsServerList :selected').text();
    var searchFilter = selectedEnvironmentServer + " " + searchBox;
    if(!isNullOrEmpty(searchBox))
    {
        serversTable.column(1).search(searchFilter).draw();
    }
    else {
        serversTable.column(1).search(selectedEnvironmentServer).draw();
    }
});
$('#allAppTypeList').change(function () {
    // regenerate table to clear filters applied
    var searchBox = $('#searchBox').val();
    selectedEnvironment = $('#allEnvironmentsList :selected').text();
    selectedAppType = $('#allAppTypeList :selected').text();
    applicationsTable.rows().search('').draw();
    var searchFilter = selectedEnvironment + " " + searchBox;
    if (!isNullOrEmpty(selectedEnvironment) && !isNullOrEmpty(searchBox)) {
        applicationsTable.column(1).search(searchFilter).column(3).search(selectedAppType).draw();
    }
    else if(!isNullOrEmpty(selectedEnvironment)&&isNullOrEmpty(searchBox))
    {
        applicationsTable.column(1).search(selectedEnvironment).column(3).search(selectedAppType).draw();
    }
    else if(isNullOrEmpty(selectedEnvironment)&&!isNullOrEmpty(searchBox))
    {
        applicationsTable.column(3).search(selectedAppType).column(1).search(searchBox).draw();
    }
    else {
        applicationsTable.rows().search(selectedAppType).draw();
    }
});
$('#appTypeSearch').change(function () {
    var teamName = sessionStorage.getItem("TeamName");

    $('#btnNewServerRequest').show();
    $('#btnNewApplicationRequest').show();
    $('#btnDataChangeRequest').show();
    $('#btnRetireRequest').show();

    $('#requesterInfo').show();

    GetAdminUsersForTeam();

    if ($('#appTypeSearch').prop('checked') === true) {
        sessionStorage.setItem("AppTypeSearch", "Servers");
        $('#allEnvironmentsList').show();
        $('#allAppTypeList').hide();
        GenerateServersTable(teamName);
    } else {
        sessionStorage.setItem("AppTypeSearch", "Applications");
        $('#allEnvironmentsList').show();
        $('#allAppTypeList').show();
        GenerateApplicationsTable(teamName);
    }
});
$("#searchBox").keyup(function () {
    applicationsTable.rows().search('').draw();
    if (!isNullOrEmpty(selectedEnvironment) && !isNullOrEmpty(selectedAppType)) {
        var searchValue = this.value + " " + selectedEnvironment;
        applicationsTable.column(1).search(searchValue).column(3).search(selectedAppType).draw();
    }
    else if (!isNullOrEmpty(selectedEnvironment)&&isNullOrEmpty(selectedAppType)) {
        var searchValue = this.value + " " + selectedEnvironment;
        applicationsTable.column(1).search(searchValue).draw();
    }
    else if (!isNullOrEmpty(selectedAppType)&&isNullOrEmpty(selectedEnvironment)) {
        applicationsTable.column(1).search(this.value).column(3).search(selectedAppType).draw();
    }
    else {
        applicationsTable.rows().search(this.value).draw();
    }
});
$("#searchBoxServers").keyup(function () {
    serversTable.rows().search('').draw();
    if (!isNullOrEmpty(selectedEnvironmentServer)) {
        var searchFilter = this.value + " " + selectedEnvironmentServer;
        serversTable.column(1).search(searchFilter).draw();
    }
    else {
        serversTable.column(1).search(this.value).draw();
    }
    //serversTable.rows().search(this.value).draw();
});
$('#btnNotesModal').click(function () {
    var divsToRemove = document.getElementsByClassName("modal-backdrop show");

    for (var i = divsToRemove.length - 1; i >= 0; i--) {
        divsToRemove[i].remove();
    }
});
function isNullOrEmpty( s ) 
{
    return ( s == null || s === "" );
}
