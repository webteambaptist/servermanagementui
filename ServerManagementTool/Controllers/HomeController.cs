﻿using Microsoft.AspNetCore.Mvc;
using ServerManagementTool.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using NLog;
using NLog.Config;
using NLog.Targets;
using ServerManagementTool.Helper;
using ServerManagementTool.Services.Interfaces;

namespace ServerManagementTool.Controllers
{
    public class HomeController : Controller
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly IServerService _serverService;

        private readonly string _user;

        public HomeController(IServerService serverService, IHttpContextAccessor httpContextAccessor)
        {
            var config = new LoggingConfiguration();
            var logFile = new FileTarget("logfile")
            { FileName = $"Logs\\ServerManagementTool-{DateTime.Today:MM-dd-yy}.log" };
            config.AddRule(NLog.LogLevel.Info, LogLevel.Fatal, logFile);
            LogManager.Configuration = config;

            _serverService = serverService;

            _user = httpContextAccessor.HttpContext.User.Identity.Name;
        }

        public async Task<IActionResult> Index()
        {
            ViewBag.Username = Authentication.cleanUsername(_user);

            var allTeams = new List<Teams>();
            var colorLegend = new List<ColorLegend>();

            try
            {
                allTeams = await _serverService.GetTeamNames();

                ViewBag.TeamNames = allTeams;

                colorLegend = await _serverService.GetColorLegend();

                ViewBag.ColorLegend = colorLegend;

                 var appTypes = await _serverService.GetAppTypes();

                ViewBag.AppTypes = appTypes;

                var environments = await _serverService.GetEnvironments();

                ViewBag.Environments = environments;

                var accessToViewAllServers = await _serverService.AccessToViewAllServers();

                ViewBag.AccessToViewAllServers = accessToViewAllServers;
            }
            catch (Exception e)
            {
                _logger.Error(
                    "Exception :: Getting Team Names :: There was an issue retrieving Team Names from Sharepoint :: " +
                    e.Message);
            }

            if (allTeams == null)
            {
                _logger.Error(
                    "Exception :: Getting Team Names :: There was an issue retrieving Team Names from Sharepoint.");

                return RedirectToAction("RequestConfirmation", "NewRequest", new { status = "Error", msg = "There was an issue retrieving data from SharePoint." });
            }

            return View();
        }


        public async Task<IActionResult> ViewAllServers()
        {
            ViewBag.Username = Authentication.cleanUsername(_user);

            var accessToViewAllServers = await _serverService.AccessToViewAllServers();
            ViewBag.AccessToViewAllServers = accessToViewAllServers;

            var colorLegend = await _serverService.GetColorLegend();

            ViewBag.ColorLegend = colorLegend;

            return View();
        }


        [HttpGet]
        public async Task<IActionResult> GetApplications(string teamName)
        {
            JsonResult jsonResult;

            try
            {
                var results = await _serverService.GetListOfApplications(teamName);

                var jsonData = JsonConvert.SerializeObject(results);

                jsonResult = Json(new { data = jsonData });
            }
            catch (Exception e)
            {
                jsonResult = null;
                _logger.Error(
                    "Exception :: GetApplications :: There was an issue retrieving Applications for Team: " + teamName + " :: " +
                    e.Message);
            }

            return jsonResult;
        }
        
        [HttpGet]
        public async Task<List<ServerDetails>> GetServerDetails(string searchParams)
        {
            List<ServerDetails> serverResults;
            var teamName = "";
            var serverName = "";

            try
            {
                var values = searchParams.Split(',');

                serverName = values[0];
                teamName = values[1];

                serverName = serverName.Replace("%20", " ");
                serverName = serverName.Replace("%3B", ";");
                teamName = teamName.Replace("%20", " ");

                serverResults = await _serverService.GetServerDetails(serverName, teamName);

                var environments = await _serverService.GetEnvironments();

                ViewBag.Environments = environments;
            }
            catch (Exception e)
            {
                serverResults = null;

                _logger.Error(
                    "Exception :: GetServerDetails :: There was an issue retrieving Server Details for Team: " + teamName + " :: Server: " + serverName + " :: " +
                    e.Message);
            }
            return serverResults;
        }

        [HttpGet]
        public async Task<List<FinalAdditionalAppDetails>> GetAdditionalAppDetails(string searchParams)
        {
            List<FinalAdditionalAppDetails> applicationDetailsResults;
            var appName = "";
            var environment = "";
            var teamName = "";
            var serverName = "";

            try
            {
                var values = searchParams.Split(',');

                appName = values[0];
                environment = values[1];
                teamName = values[2];
                serverName = values[3];

                appName = appName.Replace("%20", " ");
                teamName = teamName.Replace("%20", " ");

                serverName = serverName.Replace("%20", " ");
                serverName = serverName.Replace("%3B", ";");

                var server = serverName.Split(';');

                applicationDetailsResults = await _serverService.GetAdditionalAppDetails(appName, environment, teamName, server[0]);
            }
            catch (Exception e)
            {
                applicationDetailsResults = null;

                _logger.Error(
                    "Exception :: GetAdditionalAppDetails :: There was an issue retrieving Application Details for App: " + appName + " :: Environment: " + environment + " :: Team: " + teamName + " :: Server: " + serverName + " :: " +
                    e.Message);
            }
            return applicationDetailsResults;
        }

        [HttpGet]
        public async Task<AdminUserForTeam> GetAdminUsersForTeam(string teamName)
        {
            AdminUserForTeam adminUsers;

            try
            {
                adminUsers = await _serverService.GetAdminUsersForTeam(teamName);
            }
            catch (Exception e)
            {
                adminUsers = null;

                _logger.Error(
                    "Exception :: GetAdminUsersForTeam :: There was an issue retrieving Admin Users for Team:: " + teamName + " :: " +
                    e.Message);
            }
            return adminUsers;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllServers()
        {
            JsonResult jsonResult;

            try
            {
                var results = await _serverService.GetAllServers();
                var environments = await _serverService.GetEnvironments();

                ViewBag.Environments = environments;

                var jsonData = JsonConvert.SerializeObject(results);

                jsonResult = Json(new { data = jsonData });
            }
            catch (Exception e)
            {
                jsonResult = null;
                _logger.Error(
                    "Exception :: GetAllServers :: There was an issue retrieving all of the Servers. :: " +
                    e.Message);
            }

            return jsonResult;
        }

        [HttpGet]
        public async Task<IActionResult> GetServersByTeam(string teamName)
        {
            JsonResult jsonResult;

            try
            {
                var results = await _serverService.GetListofServersByTeam(teamName);

                var jsonData = JsonConvert.SerializeObject(results);

                var environments = await _serverService.GetEnvironments();

                ViewBag.Environments = environments;

                jsonResult = Json(new { data = jsonData });
            }
            catch (Exception e)
            {
                jsonResult = null;
                _logger.Error(
                    "Exception :: GetServersByTeam :: There was an issue retrieving Servers for Team: " + teamName + " :: " +
                    e.Message);
            }

            return jsonResult;
        }

        [HttpGet]
        public async Task<List<UpdateHeaderDetails>> GetAppDetailsForUpdate(string searchParams)
        {
            List<UpdateHeaderDetails> applicationDetailsResults;
            var appName = "";
            var environment = "";
            var teamName = "";
            var serverName = "";
            var requestType = "";

            try
            {
                var values = searchParams.Split(',');

                appName = values[0];
                environment = values[1];
                teamName = values[2];
                serverName = values[3];
                requestType = values[4];

                appName = appName.Replace("%20", " ");
                teamName = teamName.Replace("%20", " ");

                serverName = serverName.Replace("%20", " ");
                serverName = serverName.Replace("%3B", ";");

                var server = serverName.Split(';');

                requestType = requestType.Replace("%20", " ");

                applicationDetailsResults = await _serverService.GetAppDetailsForUpdate(appName, environment, teamName, server[0], requestType);
            }
            catch (Exception e)
            {
                applicationDetailsResults = null;

                _logger.Error(
                    "Exception :: GetAdditionalAppDetails :: There was an issue retrieving Application Details for App: " + appName + " :: Environment: " + environment + " :: Team: " + teamName + " :: Server: " + serverName + " :: " +
                    e.Message);
            }
            return applicationDetailsResults;
        }

        [HttpGet]
        public async Task<List<UpdateHeaderDetails>> GetServerDetailsForUpdate(string searchParams)
        {
            List<UpdateHeaderDetails> serverResults;
            var teamName = "";
            var serverName = "";
            var requestType = "";

            try
            {
                var values = searchParams.Split(',');

                serverName = values[0];
                teamName = values[1];
                requestType = values[2];

                serverName = serverName.Replace("%20", " ");
                serverName = serverName.Replace("%3B", ";");
                teamName = teamName.Replace("%20", " ");
                requestType = requestType.Replace("%20", " ");

                serverResults = await _serverService.GetServerDetailsForUpdate(serverName, teamName, requestType);

            }
            catch (Exception e)
            {
                serverResults = null;

                _logger.Error(
                    "Exception :: GetServerDetails :: There was an issue retrieving Server Details for Team: " + teamName + " :: Server: " + serverName + " :: " +
                    e.Message);
            }
            return serverResults;
        }

    }
}
