﻿using Microsoft.AspNetCore.Mvc;
using NLog;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using NLog.Config;
using NLog.Targets;
using ServerManagementTool.Helper;
using ServerManagementTool.Models;
using ServerManagementTool.Services.Interfaces;
using Microsoft.Extensions.Configuration;

namespace ServerManagementTool.Controllers
{
    public class NewRequestController : Controller
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly IServerService _serverService;

        private readonly string _user;

        public NewRequestController(IServerService serverService, IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
        {
            var config = new LoggingConfiguration();
            var logFile = new FileTarget("logfile")
                { FileName = $"Logs\\ServerManagementTool-{DateTime.Today:MM-dd-yy}.log" };
            config.AddRule(NLog.LogLevel.Info, LogLevel.Fatal, logFile);
            LogManager.Configuration = config;

            _serverService = serverService;

            _user = httpContextAccessor.HttpContext.User.Identity.Name;
        }

        [HttpGet]
        public async Task<List<HeaderDetails>> GetNewRequestHeaderInfo(string teamName, string headerType)
        {
            List<HeaderDetails> requestHeaderInfo;
            try
            {
                requestHeaderInfo = await _serverService.NewRequestHeaderInformation(teamName, headerType);
            }
            catch (Exception e)
            {
                requestHeaderInfo = null;
                
                _logger.Error(
                    "Exception :: GetServerHeaderInfo :: There was an issue retrieving Server Header Information :: " +
                    e.Message);

            }
            return requestHeaderInfo;

        }

        [HttpPost]
        public async Task<IActionResult> SubmitNewRequest(IFormCollection collection)
        {
            var submitResults = new SubmitResult();

            var userName = Authentication.cleanUsername(_user);

            var newRequest = new List<SubmitNewRequest>();

            var requestType = "";

            var userRequest = new SubmitNewRequest()
            {
                RequestKey = "RequestUser",
                RequestValue = userName
            };
            newRequest.Add(userRequest);
            
            try
            {
                foreach (var item in collection)
                {
                    switch (item.Key)
                    {
                        case "__RequestVerificationToken":
                            continue;
                        case "requestType":
                            switch (item.Value)
                            {
                                case "Server Request":
                                    requestType = "Server Request";
                                    break;
                                case "Application Request":
                                    requestType = "Application Request";
                                    break;
                                //case "Update Data Request":
                                //    requestType = "Update Data Request";
                                //    break;
                                //case "Retire/Removal Request":
                                //    requestType = "Retire/Remove Request";
                                //    break;
                            }

                            var r = new SubmitNewRequest
                            {
                                RequestKey = item.Key,
                                RequestValue = requestType
                            };
                            newRequest.Add(r);

                            break;
                        default:
                        {
                            var snr = new SubmitNewRequest
                            {
                                RequestKey = item.Key,
                                RequestValue = item.Value
                            };
                            newRequest.Add(snr);
                            break;
                        }
                    }
                }

                submitResults = await _serverService.SubmitNewRequest(newRequest);
                
                if (submitResults.Status == "success")
                {
                       return RedirectToAction("RequestConfirmation", "NewRequest", new { status = "Success", msg = "New Request Successfully submitted. We are now processing your request. The Admin will be in contact with you if they have any questions and when you request has been processed." });
                }
                else
                {
                    _logger.Error("Exception :: Submitting New Request :: Issue processing yuor new Request :: User: " + userName + " :: Request Type: " + requestType + " ::  Submit Result Stauts: "  + submitResults.Status);

                    return RedirectToAction("RequestConfirmation", "NewRequest", new { status = "Error", msg = "There was an issue submitting your request. The email was not processed." });
                }
            }
            catch (Exception e)
            {
                _logger.Error("Exception :: Submitting New Request :: Issue processing your new Request :: User: " + userName + " :: Request Type: " + requestType + " :: Submit Result Stauts: " + submitResults.Status + " :: Exception: " + e.Message);

                return RedirectToAction("RequestConfirmation", "NewRequest", new { status = "Error", msg = "There was an issue submitting your request :: User: " + userName + " :: Request Type: " + requestType + " :: Exception: " + e.Message });
            }
        }

        [HttpPost]
        public async Task<IActionResult> SubmitUpdate(IFormCollection collection)
        {
            var submitResults = new SubmitResult(); var userName = Authentication.cleanUsername(_user);

            var newRequest = new List<SubmitNewRequest>();

            var requestType = "";

            var userRequest = new SubmitNewRequest()
            {
                RequestKey = "RequestUser",
                RequestValue = userName
            };
            newRequest.Add(userRequest);

            try
            {
                foreach (var item in collection)
                {
                    switch (item.Key)
                    {
                        case "__RequestVerificationToken":
                            continue;
                        case "requestType":
                            switch (item.Value)
                            {
                                case "Server Update":
                                    requestType = "Server Update";
                                    break;
                                case "Application Update":
                                    requestType = "Application Update";
                                    break;
                            }

                            var r = new SubmitNewRequest
                            {
                                RequestKey = item.Key,
                                RequestValue = requestType
                            };
                            newRequest.Add(r);

                            break;
                        default:
                        {
                            var snr = new SubmitNewRequest
                            {
                                RequestKey = item.Key,
                                RequestValue = item.Value
                            };
                            newRequest.Add(snr);
                            break;
                        }
                    }
                }

                submitResults = await _serverService.SubmitUpdateRequest(newRequest);

                if (submitResults.Status == "success")
                {
                    return RedirectToAction("RequestConfirmation", "NewRequest",
                        new
                        {
                            status = "Success",
                            msg =
                                "Update Request Successfully submitted. We are now processing your request. The Admin will be in contact with you if they have any questions and when you request has been processed."
                        });
                }
                else
                {
                    _logger.Error("Exception :: Submitting Update Request :: Issue processing your update Request :: User: " +
                                  userName + " :: Request Type: " + requestType + " ::  Submit Result Status: " +
                                  submitResults.Status);

                    return RedirectToAction("RequestConfirmation", "NewRequest",
                        new
                        {
                            status = "Error",
                            msg = "There was an issue submitting your request. The email was not processed."
                        });
                }
            }
            catch (Exception e)
            {
                _logger.Error("Exception :: Submitting Update Request :: Issue processing yuor update Request :: User: " +
                              userName + " :: Request Type: " + requestType + " :: Submit Result Status: " +
                              submitResults.Status + " :: Exception: " + e.Message);

                return RedirectToAction("RequestConfirmation", "NewRequest", new
                {
                    status = "Error",
                    msg = "There was an issue submitting your request :: User: " + userName + " :: Request Type: " +
                          requestType + " :: Exception: " + e.Message
                });
            }
        }

        public async Task<IActionResult> RequestConfirmation(string status, string msg)
        {
            ViewBag.Status = status;
            ViewBag.Msg = msg;

            ViewBag.Username = Authentication.cleanUsername(_user);
            var accessToViewAllServers = await _serverService.AccessToViewAllServers();
            ViewBag.AccessToViewAllServers = accessToViewAllServers;

            return View();
        }
    }
}
