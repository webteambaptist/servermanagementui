﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServerManagementTool.Helper
{
    public class Authentication
    {
        public static string cleanUsername(string user)
        {
            var findSlash = user.IndexOf("\\", StringComparison.Ordinal);
            user = user.Substring(findSlash + 1);
            return user.ToUpper();
        }
    }
}
