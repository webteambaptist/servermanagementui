﻿using ServerManagementTool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ServerManagementTool.Services.Interfaces
{
    public interface IServerService
    {
        Task<List<Teams>> GetTeamNames();
        Task<List<AppTypes>> GetAppTypes();
        Task<List<Environments>> GetEnvironments();
        Task<List<Applications>> GetListOfApplications(string teamName);
        Task<List<ServerDetails>> GetServerDetails(string serverName, string teamName);
        Task<List<FinalAdditionalAppDetails>> GetAdditionalAppDetails(string appName, string environment, string teamName, string serverName);
        Task<List<HeaderDetails>> NewRequestHeaderInformation(string teamName, string headerType);
        Task<List<ColorLegend>> GetColorLegend();
        Task<AdminUserForTeam> GetAdminUsersForTeam(string teamName);
        Task<SubmitResult> SubmitNewRequest(List<SubmitNewRequest> snr);
        Task<SubmitResult> SubmitUpdateRequest(List<SubmitNewRequest> snr);
        Task<List<string>> AccessToViewAllServers();
        Task<List<Servers>> GetAllServers();
        Task<List<Servers>> GetListofServersByTeam(string teamName);

        Task<List<UpdateHeaderDetails>> GetAppDetailsForUpdate(string appName, string environment, string teamName, string serverName, string requestType);
        Task<List<UpdateHeaderDetails>> GetServerDetailsForUpdate(string serverName, string teamName, string requestType);
    }
}