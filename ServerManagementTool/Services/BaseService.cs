﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ServerManagementTool.Services
{
    public class BaseService
    {
        private readonly HttpClient _client;

        public BaseService()
        {
            try
            {
                var messageHandler = new HttpClientHandler
                {
                    UseCookies = true
                };

                _client = new HttpClient(messageHandler) {Timeout = new TimeSpan(0, 3, 0)};
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        protected async Task<TResult> GetResponse<TResult>(string serviceUri,
            Dictionary<string, string> addedHeaders = null)
        {
            if (addedHeaders != null)
            {
                foreach (var header in addedHeaders)
                {
                    if (_client.DefaultRequestHeaders.Contains(header.Key))
                        _client.DefaultRequestHeaders.Remove(header.Key);

                    _client.DefaultRequestHeaders.Add(header.Key, header.Value);
                }
            }

            try
            {
                var response = await _client.GetAsync(serviceUri);

                if (response.IsSuccessStatusCode)
                {
                    var jsonString = await response.Content.ReadAsStringAsync();
                    if (!string.IsNullOrEmpty(jsonString))
                    {
                        var returnType = typeof(TResult);
                        if (returnType == typeof(string))
                            return (TResult) Convert.ChangeType(jsonString, typeof(TResult));

                        return JsonConvert.DeserializeObject<TResult>(jsonString);
                    }
                }
            }
            catch (Exception ex)
            {
                var tags = new Dictionary<string, string> {{"serviceUri", serviceUri}};
            }

            return default(TResult);
        }

        protected async Task<TResult> PostResponse<TResult>(string serviceUri, FormUrlEncodedContent data,
            Dictionary<string, string> addedHeaders = null)
        {
            if (addedHeaders != null)
            {
                foreach (var header in addedHeaders)
                {
                    if (_client.DefaultRequestHeaders.Contains(header.Key))
                        _client.DefaultRequestHeaders.Remove(header.Key);

                    _client.DefaultRequestHeaders.Add(header.Key, header.Value);
                }
            }

            try
            {
                var response = await _client.PostAsync(serviceUri, data);

                if (response.IsSuccessStatusCode)
                {
                    var jsonString = await response.Content.ReadAsStringAsync();
                    if (!string.IsNullOrEmpty(jsonString))
                    {
                        return JsonConvert.DeserializeObject<TResult>(jsonString);
                    }
                }
            }
            catch (Exception ex)
            {
                var tags = new Dictionary<string, string> {{"serviceUri", serviceUri}};
            }

            return default(TResult);
        }
    }
}