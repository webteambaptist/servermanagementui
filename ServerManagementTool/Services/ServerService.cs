﻿using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ServerManagementTool.Services.Interfaces;
using ServerManagementTool.Models;

namespace ServerManagementTool.Services
{
    public class ServerService: BaseService, IServerService
    {
        private readonly IConfiguration _appSettingsConfig;

        public ServerService(IConfiguration appSettingsConfig)
        {
            _appSettingsConfig = appSettingsConfig;
        }

        public async Task<List<Teams>> GetTeamNames()
        {
            var serviceUri = $"{_appSettingsConfig["AppSettings:RestUrl"]}/{_appSettingsConfig["AppSettings:GetTeamNames"]}";
            var results = await GetResponse<List<Teams>>(serviceUri, new Dictionary<string, string> { });
            return results;
        }

        public async Task<List<AppTypes>> GetAppTypes()
        {
            var serviceUri = $"{_appSettingsConfig["AppSettings:RestUrl"]}/{_appSettingsConfig["AppSettings:GetAppTypes"]}";
            var results = await GetResponse<List<AppTypes>>(serviceUri, new Dictionary<string, string> { });
            return results;
        }

        public async Task<List<Environments>> GetEnvironments()
        {
            var serviceUri = $"{_appSettingsConfig["AppSettings:RestUrl"]}/{_appSettingsConfig["AppSettings:GetEnvironments"]}";
            var results = await GetResponse<List<Environments>>(serviceUri, new Dictionary<string, string> { });
            return results;
        }

        public async Task<List<Applications>> GetListOfApplications(string teamName)
        {
            var serviceUri = $"{_appSettingsConfig["AppSettings:RestUrl"]}/{_appSettingsConfig["AppSettings:GetListOfApplications"]}";
            var results = await GetResponse<List<Applications>>(serviceUri,  new Dictionary<string, string> {
                { "teamName", teamName }
            });
            return results;
        }

        public async Task<List<ServerDetails>> GetServerDetails(string serverName, string teamName)
        {
            var serviceUri = $"{_appSettingsConfig["AppSettings:RestUrl"]}/{_appSettingsConfig["AppSettings:GetServerDetails"]}";
            var results = await GetResponse<List<ServerDetails>>(serviceUri, new Dictionary<string, string> {
                { "serverName", serverName},
                { "teamName", teamName }
            });
            return results;
        }

        public async Task<List<FinalAdditionalAppDetails>> GetAdditionalAppDetails(string appName, string environment, string teamName, string serverName)
        {
            var serviceUri = $"{_appSettingsConfig["AppSettings:RestUrl"]}/{_appSettingsConfig["AppSettings:GetAdditionalAppDetails"]}";
            var results = await GetResponse<List<FinalAdditionalAppDetails>>(serviceUri, new Dictionary<string, string> {
                { "appName", appName},
                { "environment", environment },
                { "teamName", teamName },
                { "serverName", serverName }
            });
            return results;
        }

        public async Task<List<HeaderDetails>> NewRequestHeaderInformation(string teamName, string headerType)
        {
            var serviceUri = $"{_appSettingsConfig["AppSettings:RestUrl"]}/{_appSettingsConfig["AppSettings:NewRequestHeaderInformation"]}";
            var results = await GetResponse<List<HeaderDetails>>(serviceUri, new Dictionary<string, string> {
                { "teamName", teamName },
                { "headerType", headerType }
            });
            return results;
        }

        public async Task<List<ColorLegend>> GetColorLegend()
        {
            var serviceUri = $"{_appSettingsConfig["AppSettings:RestUrl"]}/{_appSettingsConfig["AppSettings:GetColorLegend"]}";
            var results = await GetResponse<List<ColorLegend>>(serviceUri, new Dictionary<string, string> { });
            return results;
        }

        public async Task<AdminUserForTeam> GetAdminUsersForTeam(string teamName)
        {
            var serviceUri = $"{_appSettingsConfig["AppSettings:RestUrl"]}/{_appSettingsConfig["AppSettings:GetAdminUsersForTeam"]}";
            var results = await GetResponse<AdminUserForTeam>(serviceUri, new Dictionary<string, string> {
                { "teamName", teamName },
            });
            return results;
        }

        public async Task<SubmitResult> SubmitNewRequest(List<SubmitNewRequest> snr)
        {
            var jsonSerializerSettings = new JsonSerializerSettings
            {
                StringEscapeHandling = StringEscapeHandling.EscapeNonAscii
            };
            var newRequest = JsonConvert.SerializeObject(snr, jsonSerializerSettings);

            var serviceUri = $"{_appSettingsConfig["AppSettings:RestUrl"]}/{_appSettingsConfig["AppSettings:SubmitNewRequest"]}";
            var results = await PostResponse<SubmitResult>(serviceUri, null, new Dictionary<string, string> {
                { "newRequest", newRequest },
            });
            return results;
        }

        public async Task<List<string>> AccessToViewAllServers()
        {
            var serviceUri = $"{_appSettingsConfig["AppSettings:RestUrl"]}/{_appSettingsConfig["AppSettings:AccessToViewAllServers"]}";
            var results = await GetResponse<List<string>>(serviceUri, new Dictionary<string, string> { });
            return results;
        }

        public async Task<List<Servers>> GetAllServers()
        {
            var serviceUri = $"{_appSettingsConfig["AppSettings:RestUrl"]}/{_appSettingsConfig["AppSettings:GetAllServers"]}";
            var results = await GetResponse<List<Servers>>(serviceUri, new Dictionary<string, string> { });
            return results;
        }

        public async Task<List<Servers>> GetListofServersByTeam(string teamName)
        {
            var serviceUri = $"{_appSettingsConfig["AppSettings:RestUrl"]}/{_appSettingsConfig["AppSettings:GetListofServersByTeam"]}";
            var results = await GetResponse<List<Servers>>(serviceUri,  new Dictionary<string, string> {
                { "teamName", teamName }
            });
            return results;
        }

        public async Task<SubmitResult> SubmitUpdateRequest(List<SubmitNewRequest> snr)
        {
            var jsonSerializerSettings = new JsonSerializerSettings
            {
                StringEscapeHandling = StringEscapeHandling.EscapeNonAscii
            };
            var updateRequest = JsonConvert.SerializeObject(snr, jsonSerializerSettings);

            var serviceUri = $"{_appSettingsConfig["AppSettings:RestUrl"]}/{_appSettingsConfig["AppSettings:SubmitUpdateRequest"]}";
            var results = await PostResponse<SubmitResult>(serviceUri, null, new Dictionary<string, string> {
                { "updateRequest", updateRequest },
            });
            return results;
        }

        public async Task<List<UpdateHeaderDetails>> GetAppDetailsForUpdate(string appName, string environment, string teamName, string serverName, string requestType)
        {
            var serviceUri = $"{_appSettingsConfig["AppSettings:RestUrl"]}/{_appSettingsConfig["AppSettings:GetDetailsForUpdate"]}";
            var results = await GetResponse<List<UpdateHeaderDetails>>(serviceUri, new Dictionary<string, string> {
                { "appName", appName},
                { "environment", environment },
                { "teamName", teamName },
                { "serverName", serverName },
                { "requestType", requestType }
            });
            return results;
        }

        public async Task<List<UpdateHeaderDetails>> GetServerDetailsForUpdate(string serverName, string teamName, string requestType)
        {
            var serviceUri = $"{_appSettingsConfig["AppSettings:RestUrl"]}/{_appSettingsConfig["AppSettings:GetDetailsForUpdate"]}";
            var results = await GetResponse<List<UpdateHeaderDetails>>(serviceUri, new Dictionary<string, string> {
                { "serverName", serverName },
                { "teamName", teamName },
                { "requestType", requestType }
            });
            return results;
        }
    }
}
